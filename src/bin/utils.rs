use std::fs;

pub fn parse_input(filename: &str) -> Vec<String> {
    let mut path: String = String::from( "inputs/");
    path.push_str(filename);
    fs::read_to_string(path).unwrap().lines().map(|x| String::from(x)).collect()
}
