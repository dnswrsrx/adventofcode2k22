use std::collections::HashMap;
mod utils;

fn create_map(input: Vec<String>) -> HashMap<(i32, i32), i32> {
    let mut height_map: HashMap<(i32, i32), i32> = HashMap::new();
    for (y, row) in input.iter().enumerate() {
        for (x, height) in row.chars().enumerate() {
            height_map.insert((x as i32, y as i32), height.to_digit(10).unwrap() as i32);
        }
    }
    height_map
}

fn is_visible(x: i32, y: i32, height_map: &HashMap<(i32, i32), i32>, max_x: i32, max_y: i32) -> bool {
    x == 0 || y == 0 || x == max_x || y == max_y ||
    (0..x).map(|v| height_map[&(v, y)]).all(|h| h < height_map[&(x, y)]) ||
    (x+1..max_x+1).map(|v| height_map[&(v, y)]).all(|h| h < height_map[&(x, y)]) ||
    (0..y).map(|v| height_map[&(x, v)]).all(|h| h < height_map[&(x, y)]) ||
    (y+1..max_y+1).map(|v| height_map[&(x, v)]).all(|h| h < height_map[&(x, y)])
}

fn count_visible(height_map: &HashMap<(i32, i32), i32>) -> i32 {
    let max_x = height_map.iter().map(|(k, _)| k.0).max().unwrap();
    let max_y = height_map.iter().map(|(k, _)| k.1).max().unwrap();
    height_map.iter().filter(|((x, y), _)| is_visible(*x, *y, height_map, max_x, max_y)).count() as i32
}

fn count_per_direction(x: i32, y: i32, range: Vec<i32>, y_axis: bool, height_map: &HashMap<(i32, i32), i32>) -> i32 {
    let coord = |v: &i32| if y_axis {(x, *v)} else {(*v, y)};

    let mut count = 0;

    for v in range.iter() {
        if &coord(v) != &(x, y) {
            count += 1;
            if height_map[&coord(v)] >= height_map[&(x, y)] {
                break;
            }
        }
    }
    count
}

fn visible_trees(x: i32, y: i32, height_map: &HashMap<(i32, i32), i32>, max_x: i32, max_y: i32) -> i32 {
    [count_per_direction(x, y, (0..y).rev().collect(), true, height_map),
     count_per_direction(x, y, (0..x).rev().collect(), false, height_map),
     count_per_direction(x, y, (y..max_y+1).collect(), true, height_map),
     count_per_direction(x, y, (x..max_x+1).collect(), false, height_map),
    ].iter().filter(|c| *c > &(0 as i32)).product()
}

fn get_most_visible(height_map: &HashMap<(i32, i32), i32>) -> i32 {
    let max_x = height_map.iter().map(|(k, _)| k.0).max().unwrap();
    let max_y = height_map.iter().map(|(k, _)| k.1).max().unwrap();
    height_map.iter().map(|((x, y), _)| visible_trees(*x, *y, height_map, max_x, max_y)).max().unwrap()
}

fn main() {
    let example = vec![
        "30373",
        "25512",
        "65332",
        "33549",
        "35390"
    ].iter().map(|v| String::from(*v)).collect();
    let height_map: HashMap<(i32, i32), i32> = create_map(example);
    assert_eq!(count_visible(&height_map), 21);
    assert_eq!(get_most_visible(&height_map), 16);

    let height_map: HashMap<(i32, i32), i32> = create_map(utils::parse_input("day8.txt"));
    assert_eq!(count_visible(&height_map), 1820);
    assert_eq!(get_most_visible(&height_map), 385112);

}
