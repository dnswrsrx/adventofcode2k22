mod utils;

fn collect_totals(calories: Vec<String>) -> Vec<usize> {
    let mut totals = Vec::new();
    let mut current_total = 0;

    for calory in calories {
        if !calory.is_empty() {
            current_total += calory.to_string().parse::<usize>().unwrap();
        } else {
            totals.push(current_total);
            current_total = 0;
        }
    }
    totals.push(current_total);

    totals.sort();
    totals.reverse();
    totals
}

fn main() {
    let example: Vec<String> = vec![
        "1000",
        "2000",
        "3000",
        "",
        "4000",
        "",
        "5000",
        "6000",
        "",
        "7000",
        "8000",
        "9000",
        "",
        "10000",
    ].into_iter().map(|x| String::from(x)).collect();

    let totals = collect_totals(example);
    assert_eq!(totals[0], 24000);
    assert_eq!(totals[..3].into_iter().sum::<usize>(), 45000);

    let totals = collect_totals(utils::parse_input(&"day1.txt"));
    assert_eq!(totals[0], 66719);
    assert_eq!(totals[..3].into_iter().sum::<usize>(), 198551)
}


