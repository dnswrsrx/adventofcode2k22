mod utils;

const ELF_HANDS: [&str; 3] = ["A", "B", "C"];
const SECOND_COL: [&str; 3] = ["X", "Y", "Z"];

fn value_to_index(choice: &str) -> usize {
    if ELF_HANDS.contains(&choice) {
        return ELF_HANDS.iter().position(|&h| h == choice).unwrap();
    }
    SECOND_COL.iter().position(|&h| h == choice).unwrap()
}

fn hand_score(choice: &str) -> usize {
    value_to_index(choice) + 1
}

fn result_score(elf_choice: &str, your_choice: &str) -> usize {
    let elf_index = value_to_index(elf_choice) as i32;
    let your_index = value_to_index(your_choice) as i32;

    match your_index - elf_index {
        0 => 3,
        1 => 6,
        -2 => 6,
        _ => 0,
    }
}

fn score_as_hands(round: &str) -> usize {
    let choices: Vec<&str> = round.split(" ").collect();
    let elf_choice = choices[0];
    let your_choice = choices[1];
    hand_score(your_choice) + result_score(elf_choice, your_choice)
}

fn hand_from_result<'a>(elf_choice: &'a str, result: &'a str) -> &'a str {
    let elf_index = value_to_index(elf_choice);

    match value_to_index(result) {
        0 => SECOND_COL[if elf_index == 0 { ELF_HANDS.len() - 1 } else {elf_index - 1}],
        1 => SECOND_COL[elf_index],
        _ => SECOND_COL[if elf_index < ELF_HANDS.len() - 1 { elf_index + 1 } else { 0 }]
    }
}

fn score_as_results(round: &str) -> usize {
    let choices: Vec<&str> = round.split(" ").collect();
    let elf_choice = choices[0];
    let result = choices[1];
    (value_to_index(result) * 3) + hand_score(hand_from_result(elf_choice, result))
}


fn main() {
    let example = [
        "A Y",
        "B X",
        "C Z"
    ];
    assert_eq!(example.iter().fold(0, |acc, r| acc + score_as_hands(r)), 15);
    assert_eq!(example.iter().fold(0, |acc, r| acc + score_as_results(r)), 12);

    let rounds = utils::parse_input("day2.txt");
    assert_eq!(rounds.iter().fold(0, |acc, r| acc + score_as_hands(r)), 11873);
    assert_eq!(rounds.iter().fold(0, |acc, r| acc + score_as_results(r)), 12014);
}
